# Hashiwokakero backend

## Overview 

This nodejs REST API is built with Express. This backend have been implemented for the Hashiwokakero project. This REST API handles clients requests and  coordinates the services of our project to solve a grid from a given picture. The services this backend will call are the Grid recognition service (https://gitlab.com/pierre.ung195/hashi-grid-struct-reco), the Digit recognition service (https://gitlab.com/Gwen02/PI_Digit_Recognition), and the Grid solver service (https://gitlab.com/RobertL31/Hashiwokakero_Services). This project is a semester project for the students of 5SDBD at INSA Toulouse.

## Installation

This backend is dockerized.
Install docker (https://www.docker.com/).
Clone the project and run : 
```
docker build -t <your-tag> .
docker run -dp 50000:50000 <your-tag>
```
The application is now listening on localhost:50000

## Implementation

This API REST only listen for the root for the POST method. The picture encoded with base 64 to process must be contained in the body of the http request in a json format ({"image": "<picture-string>"}). Then the 3 services are called sequentialy until the gri is solved or an error occurs. This backend returns a grid solved or an error (indicated which service has crashed)  

## License

Fell free to use and modify this backend :) 
MIT License. (https://github.com/expo/expo/blob/master/LICENSE)