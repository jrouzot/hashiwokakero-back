import fetch from "node-fetch"
import express from "express"
import FormData from "form-data"

import { parse_digit_solver, parse_solver_app } from './utils/parser.js'

const PORT = 50000

const IP_GRID = "10.2.5.217"
const PORT_GRID = 50001
const ROUTE_GRID = "grid"

const IP_DIGIT = "10.2.5.217"
const PORT_DIGIT = 50002
const ROUTE_DIGIT = "digit"

const IP_SOLVER = "10.2.5.217"
const PORT_SOLVER = 50003
const ROUTE_SOLVER = ""


// Create the express server instance
const app = express()

// Allow to get body of a request
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb' }));


// Get a grid picture from the mobile app
app.post("/", async (req, res) => {

    // URL of the services to call
    let url_grid = "http://" + IP_GRID + ":" + PORT_GRID + "/" + ROUTE_GRID
    let url_digit = "http://" + IP_DIGIT + ":" + PORT_DIGIT + "/" + ROUTE_DIGIT
    let url_solver = "http://" + IP_SOLVER + ":" + PORT_SOLVER + "/" + ROUTE_SOLVER


    if (req.body) {

        // Get the initial image
        let image = req.body.image

        console.log("[GRID RECOGNITION] Sending the picture to grid recognition service")


        // GRID RECOGNITION

        let response_grid = null
        let json_grid = null

        // Prepare a form data to prepare the request
        let form_grid = new FormData()
        form_grid.append('photo_b64', image)

        try {

            response_grid = await fetch(url_grid, {
                method: "POST",
                body: form_grid
            });

            // Parse the response to a js object
            json_grid = await response_grid.json()

            // Send an error if json is not correctly formatted
            if (!json_grid.grid) {

                console.log("[GRID RECOGNITION] Grid couldn't be identified, sending back an error response")
                res.json(JSON.stringify({ "status": 1, "message": "Couldn't recognize the grid, please ensure that your circles are regular and aligned" }))
                return

            }

            json_grid = await JSON.stringify(json_grid)

        } catch (e) {

            res.json(JSON.stringify({ "status": 1, "message": "Couldn't recognize the grid, please ensure that your circles are regular and aligned" }))
            return

        }

        console.log("[GRID RECOGNITION] Grid succesfully identified")

        // DIGIT RECOGNITION
        let response_digit = null
        let obj_digit = null

        console.log("[DIGIT RECOGNITION] Sending the grid to digit recognition service")

        try {

            response_digit = await fetch(url_digit, {
                method: "POST",
                body: json_grid,
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                }
            });

            obj_digit = await response_digit.json()

            // Send an error if json is not correctly formatted
            if (!obj_digit.grid) {
                console.log("[DIGIT RECOGNITION] Couldn't recognize some digits, sending back an error response")
                res.json(JSON.stringify({ "status": 2, "message": "Couldn't recognize some digit, please try to draw the grid again" }))
                return
            }

        } catch (e) {

            res.json(JSON.stringify({ "status": 2, "message": "Couldn't recognize some digit, please try to draw the grid again" }))
            return

        }

        console.log("[DIGIT RECOGNITION] All digits were succesfully identified")
        console.log("\nJSON DIGIT : \n" + JSON.stringify(obj_digit))

        let iter = 0

        while (true) {

            console.log("[SOLVER] Formatting the JSON for the solver")
            // PARSE JSON FROM DIGIT TO SOLVER
            let obj_digit_to_solver = parse_digit_solver(obj_digit, iter)

            if(obj_digit_to_solver === "error") {
                console.log("[SOLVER] Too much iterations, sending back an error response")
                res.json(JSON.stringify({ "status": 3, "message": "Couldn't solve the grid, the grid may be unsolvable or can't be identified" }))   
                return 
            }

            console.log("[SOLVER] JSON succesfully formatted")

            // We must keep track of the islands and the dimensions
            var islands = obj_digit_to_solver.grid.description

            console.log("[SOLVER] Description : " + JSON.stringify(islands))

            let json_digit_to_solver = await JSON.stringify(obj_digit_to_solver)

            let response_solver = null
            let obj_solver = null

            // SOLVER
            try {

                response_solver = await fetch(url_solver, {
                    method: "POST",
                    body: json_digit_to_solver
                });

                obj_solver = await response_solver.json()

                // Send an error if json is not correctly formatted
                if (obj_solver.connections.length === 0) {

                    console.log("[SOLVER] Grid couldn't be resolved, backtracking to try other digits")
                    iter++
                
                } else {

                    console.log("[SOLVER] Grid sucessfully solved, formatting the response")
                    console.log("\nJSON SOLVER : \n" + JSON.stringify(obj_solver))

                    console.log(islands)
                    console.log(obj_digit.grid.dimensions)
        
                    // PARSE JSON FROM SOLVER TO APP
                    let final_obj = parse_solver_app(obj_solver, islands, obj_digit.grid.dimensions)
                    let final_json = await JSON.stringify(final_obj)

                    console.log("[SOLVER] final response succesfully formatted")
                    console.log("\nJSON FINAL : \n" + final_json)
        
                    res.json(final_json)
                    return

                }

            } catch (e) {

                iter++

            }


        }

    } else {

        // TODO : ERROR HANDLING

    }

})


app.listen(PORT)

console.log(`Running on port ${PORT}`)
