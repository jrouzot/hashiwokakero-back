#!/bin/bash

# Pull the repository

sudo -u user git pull

# stop / rm the old running docker
echo -e $1 | sudo -S docker stop back_ctn
echo -e $1 | sudo -S docker rm back_ctn

# Generate the new docker image
echo -e $1 | sudo -S docker build --network=host . -t back_img

# Run the docker
echo -e $1 | sudo -S docker run -dp 80:50000 --name back_ctn back_img
