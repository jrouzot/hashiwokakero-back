export function parse_digit_solver(obj_in, iter) {

  obj_in.grid.circles.sort(sort_by_position);
  let dim = obj_in.grid.dimensions;
  let description = get_description(obj_in, iter);

  if(description === "error") {
    return "error"
  }

  let obj_out = {
    grid: {
      dimension: dim,
      description: description
    }
  };

  return obj_out;
}

function get_description(input, iter) {

  if (iter > input.grid.circles.length) {
    return "error"
  }

  let dimensions = input.grid.dimensions;

  let circles = input.grid.circles;

  if (iter === 0) {
    let digits = [];
    let j = 0;
    for (let i = 0; i < dimensions * dimensions; i++) {
      if (circles[j] && circles[j].position === i) {
        digits.push(circles[j].digit[0].val);
        j++;
      } else {
        digits.push(0);
      }
    }

    return digits;
  }

  let item_to_update = circles.slice().sort(sort_by_proba)[iter - 1];

  let digits = [];
  let j = 0;
  for (let i = 0; i < dimensions * dimensions; i++) {
    if (circles[j] && circles[j].position === i) {
      if (circles[j].position === item_to_update.position) {
        digits.push(circles[j].digit[1].val);
      } else {
        digits.push(circles[j].digit[0].val);
      }
      j++;
    } else {
      digits.push(0);
    }
  }
  return digits;
}

function sort_by_proba(a, b) {
  return a.digit[1].proba <= b.digit[1].proba;
}

function sort_by_position(a, b) {
  return a.position - b.position;
}



export function parse_solver_app(obj_in, islands, dimensions) {

  let dim = dimensions

  let bridges = sort_bridges(convert_id_to_pos(obj_in.connections, islands))

  let obj_out = {
    grid: {
      dimensions: dimensions,
      islands: []
    }
  }

  for (let i = 0; i < dim * dim; i++) {

    let item = {
      position: i,
      population: islands[i],
      connections: []
    }

    let bridges_from_i = bridges.filter(bridge => bridge[0] === i)

    let bridges_final = occur_bridges(bridges_from_i)

    bridges_final.forEach(bridge => {
      let type = "simple"
      if (bridge[2] === 2) {
        type = "double"
      }
      item.connections.push({ "to": bridge[1], "type": type })
    });

    obj_out.grid.islands.push(item)
  }

  return obj_out

}



function convert_id_to_pos(bridges, islands) {
  let islands_pos = [];

  for (let i = 0; i < islands.length; i++) {
    if (islands[i] !== 0) {
      islands_pos.push(i);
    }
  }

  let bridges_pos = [];

  bridges.forEach((bridge) => {
    let bridge_pos = [];
    bridge.forEach((island) => {
      bridge_pos.push(islands_pos[island]);
    });
    bridges_pos.push(bridge_pos);
  });

  return bridges_pos;
}


function sort_bridges(bridges) {
  let bridges_sorted = bridges.sort(function (a, b) {
    return a[0] - b[0];
  });
  return bridges_sorted
}


function compare_bridge(a, b) {
  return (a[0] === b[0] && a[1] === b[1])
}


function occur_bridges(bridges) {
  const countOccurrences = (arr, val) =>
    arr.reduce((a, v) => (compare_bridge(v, val) ? a + 1 : a), 0);

  let bridges_occur = [];

  bridges.forEach((bridge) => {
    bridges_occur.push([
      bridge[0],
      bridge[1],
      countOccurrences(bridges, bridge)
    ]);
  });


  bridges_occur = bridges_occur.map(JSON.stringify);
  bridges_occur = new Set(bridges_occur);
  bridges_occur = Array.from(bridges_occur, JSON.parse);

  return bridges_occur

}